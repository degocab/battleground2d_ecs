using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    private NavMeshAgent nma;
    private MeshRenderer meshRenderer;

    void Start()
    {

    }

    void Awake()
    {
        nma = GetComponent<UnityEngine.AI.NavMeshAgent>();

        if (nma != null)
        {
            nma.enabled = true;

        }
        //nma.SetDestination(this.transform.position);
        meshRenderer = GetComponent<MeshRenderer>();
    }
    // Update is called once per frame
    void Update()
    {
        //navMeshAgent.SetDestination(this.transform.position);
    }


    public void Convert(Entity entity, EntityManager manager, GameObjectConversionSystem conversionSystem)
    {
        //manager.AddHybridComponent(entity, typeof(NavMeshAgent));
        //manager.AddComponent(entity, typeof(CurrentLocation));
        //conversionSystem.AddHybridComponent(navMeshAgent);
        //conversionSystem.AddHybridComponent(meshRenderer);
    }
}
