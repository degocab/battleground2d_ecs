using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Experimental.AI;

public class GameHandler : MonoBehaviour
{
    public bool useECS = false;

    EntityManager manager;
    Entity enemyEntityPrefab;
    Entity allyEntityPrefab;

    [SerializeField]
    public GameObject allyPrefabECS;
    [SerializeField]
    public GameObject enemyPrefabECS;


    private static GameHandler instance;
    EntityManager entityManager;

    NavMeshQuery mapLocationQuery;

    int unitCount = 250;

    BlobAssetStore store;
    public List<GameObject> allyUnits;
    public List<GameObject> enemyUnits;

    public static GameHandler GetInstance()
    {
        return instance;
    }

    void Awake()
    {
        store = new BlobAssetStore();
        manager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, store);
        enemyEntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(enemyPrefabECS, settings);
        allyEntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(allyPrefabECS, settings);




        int indexer = 0;
        for (int i = 0; i < 15; i++)
        {
            bool isCommander = false;
            if (i % 10 == 0)
            {
                isCommander = true;
            }

            SpawnECSEntity(allyEntityPrefab, TypeEnum.Ally, isCommander, indexer);
            SpawnECSEntity(enemyEntityPrefab, TypeEnum.Enemy, isCommander, indexer);
            //SpawnECSEntity(enemyEntityPrefab, "Enemy");

        }
        var cube = GameObject.Find("Cube");
        if (cube != null)
        {
            var pp = cube.GetComponent<CubeNavMesh>();
            pp.surface.ignoreNavMeshAgent = false;
            pp.surface.BuildNavMesh();
            pp.surface.AddData();
            pp.data = pp.surface.navMeshData;

        }


    }
    // Start is called before the first frame update
    void Start()
    {
        UnityEngine.AI.NavMesh.pathfindingIterationsPerFrame = 10000;
    }

    // Update is called once per frame
    void Update()
    {

    }


    void SpawnECSEntity(Entity entityPrefab, TypeEnum type, bool isCommander, int indexer)
    {
        var entity = manager.Instantiate(entityPrefab);
        if (type == TypeEnum.Ally)
        {
            manager.AddComponent(entity, typeof(AllyTag));
            manager.AddComponent(entity, typeof(SelectedByPlayer));
        }
        else if (type == TypeEnum.Enemy)
        {
            manager.AddComponent(entity, typeof(EnemyTag));
            manager.AddComponent(entity, typeof(SelectedByAi));
        }
        manager.AddComponent(entity, typeof(UnitData));

        float3 loc = new float3(UnityEngine.Random.Range(-2f, 2f), UnityEngine.Random.Range(-2f, 2f),0f );
        manager.SetComponentData(entity, new Translation { Value = loc });

        if (isCommander)
        {
            UnitCommander unitCommander = new UnitCommander() { IsEnemy = TypeEnum.Enemy == type ? true : false };
            manager.AddComponent(entity, typeof(UnitCommander));
            manager.SetComponentData(entity, unitCommander);
        }
        UnitData unitData = new UnitData() 
        { 
            IsEnemy = TypeEnum.Enemy == type ? true : false ,
            UnitRank = isCommander ? 1 : 0,
            UniqueId = indexer,
            nation = (type == TypeEnum.Enemy ? 1 : 0),
            isReady = true,
            CurrentCommand = UnitData.CommandEnum.Attack,
            UnitType = UnitData.UnitTypeEnum.Infantry,
            health = 100,
            isMovable = true

        };



        SpriteSheetAnimationDataCust spriteSheetData = UnitAnimationCust.PlayAnimForced(/*ref prepSheetUnitPars, */unitData.playAnimationCust.baseAnimType, unitData.playAnimationCust.animDir, unitData.playAnimationCust.onComplete
                                                                                              , unitData.UnitType.ToString(), unitData.IsEnemy);
        manager.AddComponent(entity, typeof(SpriteSheetAnimationDataCust));
        manager.SetComponentData(entity, spriteSheetData);

        manager.SetComponentData(entity, unitData);


        //manager.AddComponent(entity, typeof(NavMeshAgent));
        var navMeshAgent = TypeEnum.Enemy == type ? enemyPrefabECS.GetComponent<NavMeshAgent>() : allyPrefabECS.GetComponent<NavMeshAgent>();
        navMeshAgent.enabled = true;
        navMeshAgent.SetDestination(loc);

        manager.AddComponentObject(entity, navMeshAgent);

        var meshRenderer = TypeEnum.Enemy == type ? enemyPrefabECS.GetComponent<MeshRenderer>() : allyPrefabECS.GetComponent<MeshRenderer>();
        manager.AddComponent(entity, typeof(MeshRenderer));
        manager.AddComponentObject(entity, meshRenderer);

        //ECSAnimation.PlayAnimationForced(entity, ECSUnitAnimData.BaseAnimTypeEnum.Idle, new float3(0, -1, 0), manager, default, typeEnum);
        indexer++;

        //add nation if odesnt exist
        if (unitData.nation >= DiplomacyCust.active.numberNations)
        {
            DiplomacyCust.active.AddNation(unitData.nation);
        }

    }

}
public enum TypeEnum
{
    Ally,
    Enemy
}

public struct AllyTag : IComponentData { }
public struct EnemyTag : IComponentData { }

