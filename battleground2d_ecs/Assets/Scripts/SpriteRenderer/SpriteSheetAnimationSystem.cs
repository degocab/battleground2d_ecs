using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;



/// <summary>
/// https://youtu.be/t1f8ZreCuuQ
/// </summary>
public struct SpriteSheetAnimation_Data : IComponentData
{
    public int currentFrame;
    public int frameCount;
    public float frameTimer;
    public float frameTimerMax;

    public Vector4 uv;
    public Matrix4x4 matrix;
}

public class SpriteSheetAnimation_Animate : JobComponentSystem
{
    //public float deltaTime;

    [BurstCompile]
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var deltaTime = Time.DeltaTime;
        return Entities.ForEach((ref SpriteSheetAnimation_Data spriteSheetAnimationData, ref Translation translation) => 
        {
            spriteSheetAnimationData.frameTimer += deltaTime;
            while (spriteSheetAnimationData.frameTimer >= spriteSheetAnimationData.frameTimerMax)
            {
                spriteSheetAnimationData.frameTimer -= spriteSheetAnimationData.frameTimerMax;
                //TODO: I had to mess with this to get this to work with my game last time.
                spriteSheetAnimationData.currentFrame = (spriteSheetAnimationData.currentFrame + 1) % spriteSheetAnimationData.frameCount;

                //normalized values
                float uvWidth = 1f / spriteSheetAnimationData.frameCount;// divide by num of sprites horizontally
                float uvHeight = 1f / spriteSheetAnimationData.frameCount;// divide by num of sprites vertically
                                                                          //0,0 to draw at lower left corner
                float uvOffsetX = uvWidth * spriteSheetAnimationData.currentFrame; // 1 being the position of the frame horizontally
                float uvOffsetY = 0f;// uvHeight * spriteSheetAnimationData.currentFrame; // 1 being the position of the frame vertically
                spriteSheetAnimationData.uv = new Vector4(uvWidth, uvHeight, uvOffsetX, uvOffsetY);


                //sort order
                float3 position = translation.Value;
                position.z = position.y * .01f;



                spriteSheetAnimationData.matrix = Matrix4x4.TRS(position, Quaternion.identity, Vector3.one);




            }
        }).WithBurst().Schedule(inputDeps);
        //return inputDeps;
    }
}


