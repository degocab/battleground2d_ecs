using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

/// <summary>
/// https://www.youtube.com/watch?v=tvi44I_SK3w&t=335s
/// </summary>
public class GameHandlerNew : MonoBehaviour
{

    private static GameHandlerNew instance;
    public static GameHandlerNew GetInstance()
    {
        return instance;
    }
    /// <summary>
    /// move this to each units sprite data
    /// </summary>
    public Mesh quadMesh;
    public Material walkingSpriteSheetMaterial;

    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
        EntityManager entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        EntityArchetype entityArchetype = entityManager.CreateArchetype(
            typeof(Translation),
            typeof(SpriteSheetAnimation_Data)
            );

        NativeArray<Entity> entityArray = new NativeArray<Entity>(10000, Allocator.Temp);
        entityManager.CreateEntity(entityArchetype, entityArray);

        foreach (Entity entity in entityArray)
        {
            entityManager.SetComponentData(entity,
            new Translation
            {
                Value = new float3(UnityEngine.Random.Range(-5f,5f), UnityEngine.Random.Range(-3f, 3f), 0)
            });

            entityManager.SetComponentData(entity,
                new SpriteSheetAnimation_Data
                {
                    currentFrame = UnityEngine.Random.Range(0,8),
                    frameCount = 8,
                    frameTimer = UnityEngine.Random.Range(0f,1f),
                    frameTimerMax = .1f

                });
        }

        entityArray.Dispose();

    }
}
