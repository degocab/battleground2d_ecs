using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

[UpdateAfter(typeof(SpriteSheetAnimation_Animate))]
//[UpdateInGroup(typeof(SpriteSheetAnimationSystem))]
public class SpriteSheetRenderer : ComponentSystem
{
    protected override void OnUpdate()
    {
        MaterialPropertyBlock materialPropertyBlock = new MaterialPropertyBlock();
        Vector4[] uv = new Vector4[1];
        Mesh mesh = GameHandlerNew.GetInstance().quadMesh;
        Material material = GameHandlerNew.GetInstance().walkingSpriteSheetMaterial;
        Camera mainCamera = Camera.main;

        Entities.ForEach((ref Translation translation, ref SpriteSheetAnimation_Data spriteSheetAnimationData) =>
        {
            //contains data to send to shader


            uv[0] = spriteSheetAnimationData.uv;
            materialPropertyBlock.SetVectorArray("_MainTex_UV", uv);

            
            Graphics.DrawMesh(
                mesh,
                spriteSheetAnimationData.matrix,
                material,
                0, // Layer
                mainCamera,
                0, // Submesh index
                materialPropertyBlock
            );


        });
    }
}
