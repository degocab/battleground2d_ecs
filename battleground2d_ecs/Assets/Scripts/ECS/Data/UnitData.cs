﻿using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.AI;

[GenerateAuthoringComponent]
public struct UnitData : IComponentData
{
    public bool isApproaching;
    public Entity? target;
    public float stoppingDistance;
    public float prevR;
    public float failedR;
    public float rTarget;
    public float critFailedR;
    public bool isReady;
    //public string UnitType;
    public UnitTypeEnum UnitType;
    public enum UnitTypeEnum
    {
        Infantry = 1,
        Archer = 2
    }
    public float3 direction;
    public float3 lastDirection;
    public bool isMovable;
    public bool randomFrame;
    //public List<UnitData> attackers;
    //public List<Entity> attackers;
    public int attackers; 
    public int noAttackers;
    public bool isApproachable;

    public bool IsEnemy;
    public bool isAttacking;
    public PlayAnimationCust playAnimationCust;
    public int nation;

    public int UnitRank;



    public int UniqueId;

    public CommandEnum CurrentCommand;
    public CommandEnum? PreviousCommand;

    public enum CommandEnum
    {
        Attack = 1,
        Hold = 2,
        Follow = 3,
        Move = 4

    }


    //public NavMeshAgent nma;
    public float health;
    public int maxAttackers;
    //public Entity targetEntity;
    //public string tag;
    public bool isImmune;
    public float strength;
    public float defence;
    public float nextAttack;
    public int attackRate;
    public int randomAttackRange;

    public float3 CurrPos;
    public float3 PrevPos;

    /// <summary>
    /// TODO: need to instatiate
    /// </summary>
    public float maxHealth;
    public bool isHealing;
    public bool isDying;
    public bool isSinking;

    /// <summary>
    /// TODO: initialize to 0
    /// </summary>
    public int deathCalls;
    /// <summary>
    /// TODO: initialize to 5
    /// </summary>
    public int maxDeathCalls;

    //public SpriteSheetAnimationDataCust spriteSheetData;
    //public MeshRenderer springAttractScreenRend;
    public float3 velocityVector;
    //public UnitParsTypeCust unitParsTypeCust;

    /// <summary>
    /// set after creating NavMeshAgent
    /// </summary>
    public float NavRadius;
}