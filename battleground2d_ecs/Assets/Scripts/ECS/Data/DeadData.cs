﻿using Unity.Entities;

/// <summary>
/// if unit has this, they are dead
/// </summary>
public struct DeadData : IComponentData { }
