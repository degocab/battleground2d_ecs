﻿using System.Collections.Generic;
using Unity.Entities;

public struct PlayerData : IComponentData
{
    //public <UnitData> selectedUnits;
}

/// <summary>
/// Data tag to signify that it has been selected by player
/// </summary>
public struct SelectedByPlayer : IComponentData { }
public struct SelectedByAi : IComponentData { }