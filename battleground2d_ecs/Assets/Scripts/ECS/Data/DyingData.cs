﻿using Unity.Entities;

/// <summary>
/// if unit has this, they are dying
/// </summary>
public struct DyingData : IComponentData { }