﻿using System.Collections.Generic;
using Unity.Entities;

public struct NationData : IComponentData
{
    public int Nation;
    public float targetRefreshTime;
    //public List<List<UnitData>> targets;
    //public List<KDTreeCust> targetKD;
}