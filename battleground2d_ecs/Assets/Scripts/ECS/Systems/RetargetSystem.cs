﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine.AI;
using static UnitData;

[UpdateAfter(typeof(SearchSystem))]
[DisableAutoCreation]
public class RetargetSystem : SystemBase
{
    protected override void OnUpdate()
    {
        ComponentDataFromEntity<UnitData> myTypeFromEntity = GetComponentDataFromEntity<UnitData>(true);

        Entities.ForEach((Entity entity, Translation translation, UnitData up, NavMeshAgent apprNav) =>
        {
            //UnitData up = allUnits[iRetargetPhase];
            int nation = up.nation;

            if (up.isApproaching && up.target != null && Helpers.Instance.targets[nation].Count > 0 && up.CurrentCommand != CommandEnum.Follow/*"Follow"*/)
            {
                int targetId = Helpers.Instance.targetKD[nation].FindNearest(translation.Value);
                UnitData targetUp = Helpers.Instance.targets[nation][targetId];
                Entity targetEntity = Helpers.Instance.entities[nation][targetId];
                //Translation targetTranslation = Helpers.Instance.translations[nation][targetId];

                UnitData upTarget =  myTypeFromEntity[up.target.Value];
                if (
                    targetUp.health > 0f &&
                    targetUp.attackers/*.Count*/ < targetUp.maxAttackers
                    )
                {
                    //float oldTargetDistanceSq = (targetTranslation.Value - translation.Value).sqrMagnitude;
                    //float newTargetDistanceSq = (targetTranslation.Value - translation.Value).sqrMagnitude;
                    float3 oldTargetDistanceToSq = (upTarget.CurrPos - translation.Value);
                    float oldTargetDistanceSq = (oldTargetDistanceToSq.x * oldTargetDistanceToSq.x + oldTargetDistanceToSq.y * oldTargetDistanceToSq.y + oldTargetDistanceToSq.z * oldTargetDistanceToSq.z);

                    float3 newTargetDistanceToSq = (targetUp.CurrPos - translation.Value);
                    float newTargetDistanceSq = (newTargetDistanceToSq.x * newTargetDistanceToSq.x + newTargetDistanceToSq.y * newTargetDistanceToSq.y + newTargetDistanceToSq.z * newTargetDistanceToSq.z);



                    if (newTargetDistanceSq < oldTargetDistanceSq)
                    {
                        //up.target.attackers.Remove(up);
                        //up.target.noAttackers = up.target.attackers.Count;
                        targetUp.attackers--;//.Remove(entity);
                        targetUp.noAttackers = targetUp.attackers;//.Count;

                        targetUp.attackers++;//.Add(entity);
                        targetUp.noAttackers = targetUp.attackers;//.Count;
                        //up.targetEntity = targetEntity;
                        up.target = targetEntity;
                        up.isReady = false;
                        up.isApproaching = true;
                    }
                }
            }
        }).WithoutBurst().Run();
    }
}