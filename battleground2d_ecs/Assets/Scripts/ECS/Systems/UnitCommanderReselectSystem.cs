﻿using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using static UnitData;

[DisableAutoCreation]
public class UnitCommanderReselectSystem : SystemBase
{
    protected override void OnCreate()
    {
        if (DiplomacyCust.active == null)
        {
            DiplomacyCust test = new DiplomacyCust();
        }
        if (Helpers.Instance == null)
        {
            Helpers test = new Helpers();
        }

        var cube = GameObject.Find("Cube");
        if (cube!=null)
        {
           var pp = cube.GetComponent<CubeNavMesh>();
            pp.surface.ignoreNavMeshAgent = false;
            pp.surface.BuildNavMesh();

            pp.surface.AddData();
        }

        base.OnCreate();
    }
    protected override void OnUpdate()
    {
        int unitCommanderCount = 0;
        int unitCount = 0;

        Entities.ForEach((ref UnitData unit) =>
        {
            if (unit.UnitRank == 1)
            {
                unitCommanderCount++;
            }
            else
            {
                unitCount++;
            }
        }).WithoutBurst().Run();


        //var unitCommanders = new NativeArray<UnitData>(unitCommanderCount, Allocator.Persistent);
        NativeArray<Entity> unitList = new NativeArray<Entity>(unitCount, Allocator.Persistent);

        Entities.ForEach((int entityInQueryIndex, Translation translation, UnitCommander unitCommander/*, UnitCommanderTag unitCommanderTag*/) =>
        {

            //UnitData unitCommander = unitCommanders[entityInQueryIndex];

            if (unitCommander.UnitsToCommand.Length != unitCommander.SelectedUnitPars.Length)
            {
                List<Entity> units = new List<Entity>();
                for (int c = 0; c < unitList.Length; c++)
                {
                    //UnitData currUnit = unitList[c];
                    Entity currEntity = unitList[c];
                    UnitData currUnit = GetComponent<UnitData>(currEntity); ;
                    if (currUnit.IsEnemy == unitCommander.IsEnemy)
                    {
                        if (unitCommander.UnitsToCommand.Contains(currUnit.UniqueId))
                        {
                            units.Add(currEntity);
                        }
                    }

                }

                //if (units.Count > 0)
                //{
                //    unitCommander.SelectedUnitPars = new UnitData[units.Count];
                //}
                for (int j = 0; j < units.Count(); j++)
                {
                    //UnitData pos = units[j];
                    unitCommander.SelectedUnitPars[j] = units[j];
                }

            }

        }).WithoutBurst().Run();
        unitList.Dispose();
    }
}

/// <summary>
/// component that stores unit commander data
/// </summary>
public struct UnitCommander : IComponentData 
{
    public bool IsEnemy;

    public FixedList4096<int> UnitsToCommand;
    //public UnitData[] SelectedUnitPars;
    public FixedList4096<Entity> SelectedUnitPars;

    public UnitData.CommandEnum CurrentCommand;
    public CommandEnum? PreviousCommand;
}