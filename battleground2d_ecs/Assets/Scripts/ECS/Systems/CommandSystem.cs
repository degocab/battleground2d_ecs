﻿using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine.AI;
using static UnitData;

[UpdateAfter(typeof(UnitCommanderReselectSystem))]
[DisableAutoCreation]
public class CommandSystem : SystemBase
{

    EndSimulationEntityCommandBufferSystem EndSimulationEcbSystem;
    protected override void OnCreate()
    {
        EndSimulationEcbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();

    }

    protected override void OnUpdate()
    {
        var ecb = EndSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter();



        List<NativeArray<UnitData>> unitsToCommandList = new List<NativeArray<UnitData>>();

        //get group commander commands
        //if unit is selected by player => follow player command => if unit commander find unit and has different command then unit should follow unit command?
        //if unit selected by player is unit commander => find all surrounding units in command and issue
        //if unit and unit commander selected => all follow player & set unit commander to player command => unit commander should look for any surrounding units in group

        //list of unit commanders
        List<UnitCommander> unitCommanders = new List<UnitCommander>();
        Entities.ForEach((Entity entity, int entityInQueryIndex, UnitCommander unitCommander) =>
        {
            //TODO: intialize entity with UnitCommander data on it 
            //if (unitData.UnitRank == 1)
            //{
            //    ecb.AddComponent<UnitCommander>(entityInQueryIndex, entity);

            //    unitCommanders.Add(GetComponent<UnitCommander>(entity));

            //}
            unitCommanders.Add(unitCommander);
        }).WithoutBurst().Run();

        for (int i = 0; i < unitCommanders.Count; i++)
        {

            var unitCommander = unitCommanders[i];

            if (unitCommander.CurrentCommand != unitCommander.PreviousCommand)
            {
                //var listOfUnitsToAdd = new List<UnitData>();
                var listOfUnitsToAdd = new NativeArray<UnitData>(unitCommander.UnitsToCommand.Length, Allocator.Persistent);
                int counter = 0;
                Entities.ForEach((int entityInQueryIndex, UnitData unitData) =>
                {

                    if (unitCommander.UnitsToCommand.Contains(unitData.UniqueId))
                    {
                        //listOfUnitsToAdd.Add(unitData);
                        listOfUnitsToAdd[counter] = unitData;
                        counter++;
                    }

                }).WithoutBurst().Run();
                unitsToCommandList.Add(listOfUnitsToAdd);
                unitCommander.PreviousCommand = unitCommander.CurrentCommand;
                listOfUnitsToAdd.Dispose();
            }
        }

        EntityQuery queryForPlayerSelectedCount = GetEntityQuery(typeof(SelectedByPlayer));
        EntityQuery queryForAiSelectedCount = GetEntityQuery(typeof(SelectedByAi));

        NativeArray<UnitData> selectedPlayerUnits = new NativeArray<UnitData>(queryForPlayerSelectedCount.CalculateEntityCount(), Allocator.Persistent);
        NativeArray<UnitData> selectedAiUnits = new NativeArray<UnitData>(queryForAiSelectedCount.CalculateEntityCount(), Allocator.Persistent);
        int counterIndex = 0;
        Entities.ForEach((UnitData unitData, SelectedByPlayer selectedByPlayer) =>
        {

            selectedPlayerUnits[counterIndex] = unitData;
            counterIndex++;

        }).WithoutBurst().Run();
        counterIndex = 0;
        Entities.ForEach((UnitData unitData, SelectedByAi selectedByAi) =>
        {

            selectedAiUnits[counterIndex] = unitData;
            counterIndex++;

        }).WithoutBurst().Run();

        //TODO add logic for AI
        unitsToCommandList.Add(selectedPlayerUnits);
        unitsToCommandList.Add(selectedAiUnits);

        UnitData playerDataToResuse = new UnitData();
        UnitData aiDataToResuse = new UnitData();
        Entities.ForEach((PlayerData playerData, UnitData unitData) => { playerDataToResuse = unitData; }).WithoutBurst().Run();
        Entities.ForEach((AiData aiData, UnitData unitData) => { aiDataToResuse = unitData; }).WithoutBurst().Run();



        //var tupleList = new List<Tuple<UnitData, NavMeshAgent>>();
        //Entities.ForEach((Entity entity, UnitData unitData, NavMeshAgent nma) =>
        //{
        //    tupleList.Add(new Tuple<UnitData, NavMeshAgent>(unitData, nma));
        //}).WithoutBurst().Run();

        var entityQuery = GetEntityQuery(typeof(UnitData), typeof(Translation), typeof(NavMeshAgent));

        NavMeshAgent[] agents = entityQuery.ToComponentArray<NavMeshAgent>();




        //TODO: find a better way to do commands between player/ai?
        //TODO: put this in a job => set a data component on units that need commanded with unit commader/player id and command value 
        //foreach (var units in unitsToCommandList)
        //NativeArray<UnitData> unitsToCommand = new NativeArray<UnitData>(Allocator.Persistent);

        for (int j = 0; j < unitsToCommandList.Count; j++)
        {
            UnitCommander? unitCommander = null;
            if (j < unitCommanders.Count)
            {
                //unitsToCommand = playerControl.selectedUnits;
                NativeArray<UnitData> unitsToCommand = unitsToCommandList[j];

                //unit commander
                //should be in the same order as above where we add list of units
                unitCommander = unitCommanders[j];
                if (unitsToCommand != null && unitsToCommand.Length > 0)
                {
                    for (int i = 0; i < unitsToCommand.Length; i++)
                    {
                        UnitData unit = unitsToCommand[i];

                        //string commandToFollow = "";
                        CommandEnum? commandToFollow = null;


                        //TODO: see if this is where the unit commander commands should go
                        //if (unitCommander != null)
                        //{
                        //    commandToFollow = "Attack";//unitCommander.CurrentCommand; 
                        //}



                        if (unit.nation == Helpers.Instance.PlayerNation)
                        {
                            commandToFollow = CommandEnum.Attack;//playerDataToResuse.CurrentCommand;
                        }
                        else if (unit.nation == 0)
                        {
                            commandToFollow = CommandEnum.Attack;//aiDataToResuse.CurrentCommand;
                        }

                        if (unit.CurrentCommand != commandToFollow)
                        {
                            unit.CurrentCommand = commandToFollow.Value;
                            unit.PreviousCommand = commandToFollow;

                            //get nma from tuple list 
                            NavMeshAgent nma = agents[0];//tupleList.FirstOrDefault(x => x.Item1.UniqueId == unit.UniqueId).Item2;

                            switch (commandToFollow)
                            {
                                default:
                                case CommandEnum.Hold/*"Hold"*/:
                                    unit.isApproachable = true;
                                    unit.isApproaching = false;
                                    unit.isAttacking = false;
                                    unit.isReady = true;
                                    nma.isStopped = true;
                                    break;
                                case CommandEnum.Move/*"Move"*/:
                                    //get player location to move to?
                                    nma.isStopped = false;
                                    break;
                                case CommandEnum.Attack/*"Attack"*/:
                                    nma.isStopped = false;
                                    break;
                                case CommandEnum.Follow/*"Follow"*/:
                                    nma.isStopped = false;
                                    unit.isApproaching = true;
                                    //if (unitCommander != null)
                                    //{
                                    //    unit.target = unitCommander;
                                    //}
                                    //else
                                    //{
                                    if (!unit.IsEnemy)
                                    {
                                        unit.target = null;//playerDataToResuse;
                                    }
                                    //}
                                    break;
                            }
                        }
                        else
                        {
                            unit.CurrentCommand = unit.PreviousCommand.Value;
                        }

                    }
                }
                //unitsToCommand.Dispose();

            }

        }

        selectedPlayerUnits.Dispose();
        selectedAiUnits.Dispose();

    }
}