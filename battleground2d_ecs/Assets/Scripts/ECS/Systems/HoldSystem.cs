﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Entities;
using UnityEngine.AI;
using static UnitData;

[UpdateAfter(typeof(CommandSystem))]
[DisableAutoCreation]
public class HoldSystem : SystemBase
{
    protected override void OnUpdate()
    {
        Entities.ForEach((UnitData unitData, NavMeshAgent nma) =>
        {
            if (unitData.CurrentCommand == CommandEnum.Hold/*"Hold"*/)
            {
                unitData.isApproachable = true;
                unitData.isApproaching = false;
                unitData.isAttacking = false;
                unitData.isReady = true;
                nma.isStopped = true;
            }

        }).WithoutBurst().Run();    
    }
}