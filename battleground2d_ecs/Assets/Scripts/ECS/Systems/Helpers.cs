﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class Helpers
{
    private static Helpers _Instance;
    public static Helpers Instance
    {
        get
        {
            if (_Instance == null)
                _Instance = new Helpers();
            return _Instance;
        }
    }

    /// <summary>
    /// TODO: This needs to be in its own class
    /// </summary>
    public List<List<UnitData>> targets = new List<List<UnitData>>();

    public List<float> targetRefreshTimes = new List<float>();
    public List<KDTreeCust> targetKD = new List<KDTreeCust>();
    public List<List<Entity>> entities = new List<List<Entity>>();
    public List<List<Translation>> translations = new List<List<Translation>>();


    public int UnitCount { get; set; }
    public int PlayerNation = 1;

    public bool CanHitCoordinateOld(Vector3 shooterPosition, Vector3 targetPosition, Vector3 targetVolocity, float launchSpeed, float distanceIncrement)
    {


        //TODO: DO A  CHECK ON WETHER WE ARE PERPENDICULAR OR NOT

        bool canHit = false;

        float vini = launchSpeed;
        float g = 9.81f;


        Vector3 shootPosition2d = new Vector3(shooterPosition.x, shooterPosition.y, 0);
        Vector3 targetPosition2d = new Vector3(targetPosition.x, targetPosition.y, 0);

        float rTarget2d = (targetPosition2d - shootPosition2d).magnitude;
        rTarget2d = rTarget2d + distanceIncrement * rTarget2d;
        float sqrt = (vini * vini * vini * vini) - (g * (g * (rTarget2d * rTarget2d) + 2 * (targetPosition.y - shooterPosition.y) * (vini * vini)));

        if (/*sqrt >= 0 &&*/
            ((shootPosition2d.x <= targetPosition.x + 1 && shootPosition2d.x >= targetPosition2d.x - 1)
             || (shootPosition2d.y <= targetPosition2d.y + 1 && shootPosition2d.y >= targetPosition2d.y - 1)))
        {
            canHit = true;
        }

        return canHit;
    }


    public bool CanHitCoordinate(float3 shooterPosition, float3 targetPosition, float3 targetVolocity, float launchSpeed, float distanceIncrement)
    {


        //TODO: DO A  CHECK ON WETHER WE ARE PERPENDICULAR OR NOT

        bool canHit = false;

        float vini = launchSpeed;
        float g = 9.81f;


        float3 shootPosition2d = new float3(shooterPosition.x, shooterPosition.y, 0);
        float3 targetPosition2d = new float3(targetPosition.x, targetPosition.y, 0);


        //calc magnitude
        float3 rTargfloat = (targetPosition2d - shootPosition2d);


        float rTarget2d = math.sqrt(rTargfloat.x * rTargfloat.x + rTargfloat.y * rTargfloat.y + rTargfloat.z * rTargfloat.z); ;
        rTarget2d = rTarget2d + distanceIncrement * rTarget2d;
        float sqrt = (vini * vini * vini * vini) - (g * (g * (rTarget2d * rTarget2d) + 2 * (targetPosition.y - shooterPosition.y) * (vini * vini)));

        if (/*sqrt >= 0 &&*/
            ((shootPosition2d.x <= targetPosition.x + 1 && shootPosition2d.x >= targetPosition2d.x - 1)
             || (shootPosition2d.y <= targetPosition2d.y + 1 && shootPosition2d.y >= targetPosition2d.y - 1)))
        {
            canHit = true;
        }

        return canHit;
    }



    public void AddNation(int nation)
    {
        targets.Add(new List<UnitData>());
        targetRefreshTimes.Add(-1f);

        EntityManager manager = World.DefaultGameObjectInjectionWorld.EntityManager;
        EntityArchetype archetype = manager.CreateArchetype(typeof(NationData));

        Entity nationEntity = manager.CreateEntity(archetype);
        manager.AddComponent(nationEntity, typeof(NationData));
        manager.SetComponentData(nationEntity, new NationData() {targetRefreshTime = -1, Nation = nation});

        targetKD.Add(null);
    }

    public void LaunchArrowDelay(UnitData pars, UnitData targPars, float3 launchPoint, UnitParsTypeCust unitParsTypeCust)
    {
        LaunchArrow(pars, targPars, launchPoint, unitParsTypeCust);
    }
    public void LaunchArrow(UnitData? attPars, UnitData? targPars, Vector3 launchPoint, UnitParsTypeCust unitParsTypeCust)
    {
        if ((attPars != null) && (targPars != null))
        {
            LaunchArrowInner(attPars, targPars, launchPoint, false, unitParsTypeCust);

        }
    }

    public void LaunchArrowInner(UnitData? attPars, UnitData? targPars, Vector3 launchPoint1, bool isCosmetic, UnitParsTypeCust unitParsTypeCust)
    {
        Quaternion rot = new Quaternion(0f, 0.0f, 0.0f, 0.0f);
        Vector3 launchPoint = launchPoint1 + Vector3.zero;


        if (attPars != null && targPars != null)
        {
            Vector3 arrForce2 = LaunchDirection(launchPoint, targPars.Value.CurrPos, targPars.Value.velocityVector, unitParsTypeCust.velArrow);
            float failureError = 0f;

            if (unitParsTypeCust.arrow != null)
            {
                ArrowParsCust arp = unitParsTypeCust.arrow.GetComponent<ArrowParsCust>();
                if (arp != null)
                {

                }
            }



            float magBeforeError = arrForce2.magnitude;


            var rand = UnityEngine.Random.insideUnitSphere;
            arrForce2 = arrForce2 + new Vector3(rand.x, rand.y, 0) * arrForce2.magnitude * failureError;
            arrForce2 = arrForce2.normalized * magBeforeError;

            arrForce2.z = 0f;
            if ((arrForce2.sqrMagnitude > 0.0f) && (arrForce2.y != -Mathf.Infinity) && (arrForce2.y != Mathf.Infinity))
            {
                if (unitParsTypeCust.arrow != null)
                {
                    GameObject arroww = (GameObject)UnityEngine.Object.Instantiate(unitParsTypeCust.arrow, launchPoint, rot);
                    //arroww.GetComponent<>

                    ArrowParsCust arrPars = arroww.GetComponent<ArrowParsCust>();

                    if (arrPars != null)
                    {
                        arrPars.attPars = attPars;
                        arrPars.targPars = targPars;



                        //set random tolerance
                        Vector3 posWithTolerance = targPars.Value.CurrPos;
                        posWithTolerance.x = posWithTolerance.x + UnityEngine.Random.Range(-.2f, .2f);
                        posWithTolerance.y = posWithTolerance.y + UnityEngine.Random.Range(-.2f, .2f);

                        arrPars.targetPos = posWithTolerance;
                        arrPars.Init(1.5f, arrForce2);

                    }
                }
            }

        }


    }


    public Vector3 LaunchDirection(Vector3 shooterPosition, Vector3 targetPosition, Vector3 targetVelocity, float launchSpeed)
    {
        float vini = launchSpeed;


        // horizontal plane projections	
        Vector3 shooterPosition2d = new Vector3(shooterPosition.x, 0f, shooterPosition.z);
        Vector3 targetPosition2d = new Vector3(targetPosition.x, 0f, targetPosition.z);

        float Rtarget2d = (targetPosition2d - shooterPosition2d).magnitude;

        //shooter and target coordinates
        float ax = shooterPosition.x;
        float ay = shooterPosition.y;
        float az = 0;

        float tx = targetPosition.x;
        float ty = targetPosition.y;
        float tz = 0;

        float g = 9.81f;

        float sqrt = (vini * vini * vini * vini) - (g * (g * (Rtarget2d * Rtarget2d) + 2 * (ty - ay) * (vini * vini)));
        sqrt = Mathf.Sqrt(sqrt);

        float angleInRadians = Mathf.Atan((vini * vini + sqrt) / (g * Rtarget2d));
        float angleInDegrees = angleInRadians * Mathf.Rad2Deg;

        if (angleInDegrees > 45f)
        {
            angleInDegrees = 90f - angleInDegrees;
        }

        if (angleInDegrees < 0f)
        {
            angleInDegrees = -angleInDegrees;
        }

        Vector3 rotAxis = Vector3.Cross((targetPosition - shooterPosition), new Vector3(0f, 1f, 0f));
        Vector3 arrForce = (GenericMath.RotAround(-angleInDegrees, (targetPosition - shooterPosition), rotAxis)).normalized;

        // shoting time
        float shTime = Mathf.Sqrt(
            ((tx - ax) * (tx - ax) + (tz - az) * (tz - az)) /
            ((vini * arrForce.x) * (vini * arrForce.x) + (vini * arrForce.z) * (vini * arrForce.z))
        );

        Vector3 finalDirection = vini * arrForce + 0.5f * shTime * targetVelocity;
        return finalDirection;
    }


}


public class DiplomacyCust 
{
    //public static DiplomacyCust active;

    private static DiplomacyCust _Instance;

    public static DiplomacyCust active
    {
        get
        {
            if (_Instance == null)
            {
                _Instance = new DiplomacyCust();
                _Instance.SetAllWar();
            }
            return _Instance;
        }
    }


    [HideInInspector] public int numberNations;
    public List<List<int>> relations = new List<List<int>>();
    public int playerNation = 0;

    public int defaultRelation = 1;

    void Awake()
    {
        //active = this;
    }

    void Start()
    {
        SetAllWar();
    }

    public void AddNation(int nation)
    {
        for (int i = 0; i < numberNations; i++)
        {
            relations[i].Add(defaultRelation);
        }

        relations.Add(new List<int>());

        for (int i = 0; i < numberNations + 1; i++)
        {
            relations[numberNations].Add(defaultRelation);
        }

        numberNations++;

        Helpers.Instance.AddNation(nation);
    }

    public void SetAllPeace()
    {
        for (int i = 0; i < numberNations; i++)
        {
            for (int j = 0; j < numberNations; j++)
            {
                if (i != j)
                {
                    relations[i][j] = 0;
                }
            }
        }

        //Debug.Log("peace");
    }

    public void SetAllWar()
    {
        for (int i = 0; i < numberNations; i++)
        {
            for (int j = 0; j < numberNations; j++)
            {
                if (i != j)
                {
                    relations[i][j] = 1;
                }
            }
        }

        // Debug.Log("war");
    }
}

