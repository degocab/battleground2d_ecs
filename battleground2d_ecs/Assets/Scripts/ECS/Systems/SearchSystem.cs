﻿
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using static UnitData;

[UpdateAfter(typeof(HoldSystem))]
[DisableAutoCreation]
public class SearchSystem : SystemBase
{
    protected override void OnUpdate()
    {

        float deltaTime = Time.DeltaTime;

        //get nation count and create array to store for future use
        EntityQuery query = GetEntityQuery(ComponentType.ReadOnly<NationData>());
        int count = query.CalculateEntityCount();
        NativeArray<NationData> nations = new NativeArray<NationData>(count, Allocator.Persistent);

        nations = query.ToComponentDataArray<NationData>(Allocator.Persistent);


        Entities.ForEach((int entityInQueryIndex, NationData nation) =>
        {
            nation.targetRefreshTime -= deltaTime;

            //if its time to refresh target
            if (nation.targetRefreshTime < 0f)
            {

                nation.targetRefreshTime = 1f;
                nations[entityInQueryIndex] = nation;
                //loop through untis
            }

        }).WithoutBurst().Run();
        for (int i = 0; i < nations.Length; i++)
        {

            if (nations[i].targetRefreshTime < 0f)
            {

                List<UnitData> nationTargets = new List<UnitData>();
                List<Entity> nationEntities = new List<Entity>();
                List<Translation> nationTargetPositions = new List<Translation>();

                Entities.ForEach((Entity e, UnitData up, Translation translation) =>
                {
                    up.CurrPos = translation.Value;

                    if (
                        up.nation != i && // not sure why this is checking against the target refresh time
                        up.isApproachable &&
                        up.health > 0f && // if still alive
                        up.attackers/*.Count*/ < up.maxAttackers // if not reach trhe max attackers
                        && DiplomacyCust.active.relations[up.nation][i] == 1 //- I need to manage my own ally vs enemy 
                        )
                    {
                        nationTargets.Add(up);
                        nationEntities.Add(e);
                        nationTargetPositions.Add(translation);
                    }

                }).WithoutBurst().Run();

                //TODO: create nation component data to store array of targets
                //TODO: check where to initialize?
                Helpers.Instance.targets[i] = nationTargets;
                Helpers.Instance.entities[i] = nationEntities;
                Helpers.Instance.translations[i] = nationTargetPositions;

                //TODO: create nation component data property to store KDTree data
                NativeArray<Translation> nativeArrayToCopyToVector3Array = new NativeArray<Translation>(nationTargetPositions.Count, Allocator.Persistent);
                if (nationTargetPositions.Any())
                {
                    for (int j = 0; j < nationTargetPositions.Count; j++)
                    {
                        nativeArrayToCopyToVector3Array[j] = nationTargetPositions[j];
                    }
                }
                Vector3[] v3Array = new Vector3[nativeArrayToCopyToVector3Array.Count()];
                nativeArrayToCopyToVector3Array.Reinterpret<Vector3>().CopyTo(v3Array);
                //TODO: make this into a job
                Helpers.Instance.targetKD[i] = KDTreeCust.MakeFromPoints(v3Array);

                nativeArrayToCopyToVector3Array.Dispose();
            }
        }



        Entities.ForEach((Entity entity, int entityInQueryIndex, UnitData up, Translation translation) =>
        {
            int nation = up.nation;
            if (up.isReady && Helpers.Instance.targets[nation].Count > 0 && (new List<CommandEnum> { CommandEnum.Attack }).Contains(up.CurrentCommand))
            {
                int targetId = Helpers.Instance.targetKD[nation].FindNearest(translation.Value);
                UnitData targetUp = Helpers.Instance.targets[nation][targetId];
                Entity targetEntity = Helpers.Instance.entities[nation][targetId];
                Translation targetTranslation = Helpers.Instance.translations[nation][targetId];
                if (
                    targetUp.health > 0f &&
                    targetUp.attackers/*.Count*/ < targetUp.maxAttackers
                    )
                {
                    targetUp.attackers++;//.Add(entity);
                    targetUp.noAttackers = targetUp.attackers/*.Count*/;
                    up.target = targetEntity;
                    //up.targetEntity = targetEntity;



                    var direction = targetTranslation.Value - translation.Value;//targetUp.transform.position - up.transform.position;
                    up.direction = direction;

                    up.isReady = false;
                    up.isApproaching = true;
                }
            }
        }).WithoutBurst().Run();

        nations.Dispose();

    }
}